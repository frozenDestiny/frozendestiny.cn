import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv} from 'vite'
import vue from '@vitejs/plugin-vue'
import { createStyleImportPlugin, ElementPlusResolve } from 'vite-plugin-style-import'
import VueDevTools from 'vite-plugin-vue-devtools'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VueDevTools(),
    createStyleImportPlugin({
      resolves: [ElementPlusResolve()],
      libs: [
        {
          libraryName: 'element-plus',
          esModule: true,
          ensureStyleFile: true,
          resolveStyle: (name) => {
            return `element-plus/theme-chalk/${name}.css`
          },
          resolveComponent: (name) => {
            return `element-plus/lib/${name}`;
          },
        }
      ]
    }),
  ],
  assetsDir: 'assets',
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "@/styles/common.scss";'
      }
    }
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '~': fileURLToPath(new URL('./', import.meta.url)),
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      'api': fileURLToPath(new URL('src/api', import.meta.url)),
      "util": fileURLToPath(new URL('src/common/util', import.meta.url)),
    }
  },
  optimizeDeps: {
    include: [
      'swiper',
      'highlight.js/lib/core',
      'highlight.js/lib/languages/go',
      'highlight.js/lib/languages/css',
      'highlight.js/lib/languages/sql',
      'highlight.js/lib/languages/php',
      'highlight.js/lib/languages/xml',
      'highlight.js/lib/languages/json',
      'highlight.js/lib/languages/bash',
      'highlight.js/lib/languages/less',
      'highlight.js/lib/languages/scss',
      'highlight.js/lib/languages/yaml',
      'highlight.js/lib/languages/rust',
      'highlight.js/lib/languages/java',
      'highlight.js/lib/languages/shell',
      'highlight.js/lib/languages/nginx',
      'highlight.js/lib/languages/stylus',
      'highlight.js/lib/languages/python',
      'highlight.js/lib/languages/javascript',
      'highlight.js/lib/languages/typescript'
    ],
    exclude: [
      '@vue/compiler-sfc',
    ]
  },
  build: {
    brotliSize: false,
    rollupOptions: {
      output: {
        entryFileNames: '[name]-[hash].js',
        chunkFileNames: '[name]-[hash].js',
        assetFileNames: '[name]-[hash].[ext]',
        manualChunks(id) {
          if (id.includes('/node_modules/')) {
            const expansions = ['swiper', 'wangeditor', 'highlight.js',]
            if (expansions.some(exp => id.includes(`/node_modules/${exp}`))) {
              return 'expansion'
            } else {
              return 'vendor'
            }
          }
        }
      }
    }
  },
  server: {
    port: '8089',
    host: 'localhost',
    open: true,
    proxy: {
      '/api': {
        target: '',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      }
    }
  }
})
