export const NAV_MENU = [
    {
        menuName: "首页",
        menuRouter: "/index",
        menuIcon: "iconshouye",
    },
    {
        menuName: "文章",
        menuRouter: "/articleList",
        menuIcon: "iconarticle-line",
    },
    {
        menuName: "想法",
        menuRouter: "/idea",
        menuIcon: "iconideas_fill",
    },
    {
        menuName: "相册",
        menuRouter: "/album",
        menuIcon: "iconpicture",
    },
    {
        menuName: "留言",
        menuRouter: "/message-board",
        menuIcon: "iconliuyan",
    },
]