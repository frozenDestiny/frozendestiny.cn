
export const HTTP_TIMEOUT = 10000 // http请求超时时间
export const HTTP_SUCCESS_CODE = 0 // http请求成功code--服务端业务code码
export const SERVER_URL = import.meta.env.VITE_API_ONLINE_URL // api baseurl
export const TOAST_DURATION = 3000 // toast框默认持续时间
export const PAGE_SIZE = 10 // 默认每页内容数
export const LINKS = Object.freeze({
    VUE: 'https://v3.vuejs.org/',
    GIN: 'https://gin-gonic.com/'
})
export const DEFAULT_INFO = Object.freeze({
    AVATAR: 'src/assets/imgs/avatar.jpg',
    NAME: 'Frozen Dsetiny',
    WEIXIN: 'FrozenDestiny0916',
    QQ: 62375700
})
export const VERSE_LIST = [
    {
        verseSentence: '一箫一剑平生意，负尽狂名十五年。'
    },
    {
        verseSentence: '粗缯大布裹生涯，腹有诗书气自华。'
    },
    {
        verseSentence: '当时明月在，曾照彩云归。'
    },
    {
        verseSentence: '伤心桥下春波绿，曾是惊鸿照影来。'
    },
    {
        verseSentence: '醉后不知天在水，满船清梦压星河。'
    },
    {
        verseSentence: '我与春风皆过客，你携秋水揽星河。'
    },
    {
        verseSentence: '应是天仙狂醉，乱把白云揉碎。'
    },
    {
        verseSentence: '唤起一天明月，照我满怀冰雪。'
    },
    {
        verseSentence: '我见青山多妩媚，料青山，见我应如是。'
    },
    {
        verseSentence: '落花人独立，微雨燕双飞。'
    }
    
]
export const LINKS_INFO = [
    {
        title: 'github',
        className: 'icongithub',
        link: 'https://github.com/frozendestiny'
    },
    {
        title: '新浪微博',
        className: 'iconweibo1',
        link: 'https://weibo.com/u/2723914334'
    },
    {
        title: '微信',
        className: 'iconxiangmulan-weixinhao',
        isWeixin: 1
    },
    {
        title: 'QQ',
        className: 'iconqq',
        isQQ: 1
    },
    {
        title: '邮箱',
        className: 'iconmailfillicon',
        link: 'mailto: lz199308@sina.com'
    },
    {
        title: '哔哩哔哩',
        className: 'icontv',
        link: 'https://space.bilibili.com/361711744'
    },
    {
        title: '网易云音乐',
        className: 'iconnetease',
        link: 'https://music.163.com/#/user/home?id=355873261'
    },
    {
        title: '知乎',
        className: 'iconzhihu',
        link:'https://www.zhihu.com/people/liu-zhen-27-90'
    }

]

export const SWIPER_LIST = [
    {
        ideaName: '想法1',
        ideaContainer: '想法内容想法内容想法内容想法内容想法内容想法内容',
        ideaBg: 'src/assets/imgs/baolei-2.jpg'
    },
    {
        ideaName: '想法2',
        ideaContainer: '想法内容想法内容想法内容想法内容想法内容想法内容',
        ideaBg: 'src/assets/imgs/baolei-3.jpg'
    },
    {
        ideaName: '想法3',
        ideaContainer: '想法内容想法内容想法内容想法内容想法内容想法内容',
        ideaBg: 'src/assets/imgs/baolei-4.jpg'
    }
]

export const NOTICE = {
    content: 'Welcome to my world.'
}

export const TagsList = [
    {
        className: 'iconNGINX',
        tagName: 'nginx',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconqianduan',
        tagName: '前端',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconshujuku',
        tagName: '数据库',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconSass',
        tagName: 'sass',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconhttp',
        tagName: 'http',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconshenghuo-copy',
        tagName: '生活',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconvue',
        tagName: 'vue',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconjavascript',
        tagName: 'js',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'icongolang',
        tagName: 'go',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconyouxi',
        tagName: '游戏',
        tagNum: '10',
        tagLink: '',
    },
    {
        className: 'iconwebpack',
        tagName: 'webpack',
        tagNum: '10',
        tagLink: '',
    },
]
