// import * as UI from './ui'

/**
 * url拼接参数
 * @param url 原始url
 * @param obj 需要拼接的对象
 * @returns {*}
 */
export function parseGetParams (url, obj) {
  let i = 0
  let hasParam = false
  if (url.indexOf('?') !== -1) {
    hasParam = true
  }
  for (let key in obj) {
    if (obj[key] === null || obj[key] === undefined) {
      continue
    }
    if (i === 0 && !hasParam) {
      url += '?' + encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])
    } else {
      url += '&' + encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])
    }
    i++
  }
  return url
}

export function getSgObj (key) {
  let obj = {}
  obj = window.sessionStorage.getItem(key)
  return JSON.parse(obj)
}

export function setSgObj (key, value) {
  return window.sessionStorage.setItem(key, JSON.stringify(value))
}

export function getSg (key) {
  return window.sessionStorage.getItem(key)
}

export function setSg (key, value) {
  window.sessionStorage.setItem(key, value)
}

export function removeSg (key) {
  window.sessionStorage.removeItem(key)
  window.sessionStorage.setItem(key, '')
}

export function getLgObj (key) {
  let obj = window.localStorage.getItem(key)
  return JSON.parse(obj)
}

export function setLgObj (key, value) {
  return window.localStorage.setItem(key, JSON.stringify(value))
}

export function getLg (key) {
  return window.localStorage.getItem(key)
}

export function setLg (key, value) {
  return window.localStorage.setItem(key, value)
}

export function removeLg (key) {
  return window.localStorage.removeItem(key)
}

export function isWeiXin () {
  let ua = window && window.navigator.userAgent.toLowerCase()
  if (ua.match(/MicroMessenger/i) == 'micromessenger') {
    return true
  } else {
    return false
  }
}

export function checkMobileSystem () {
  let ua = navigator.userAgent.toLowerCase()
  if (/iphone|ipad|ipod/.test(ua)) {
    return 'ios'
  } else if (/android/.test(ua)) {
    return 'android'
  }
  return ''
}
/**
 *
 * @param num 当前周的前n个周(n为负数)或后n个周(n为正数)或0
 * @returns {Date}
 */
export function getWeekStartDate (num) {
  num = num || 0
  let now = new Date() // 当前日期
  let nowDayOfWeek = now.getDay() // 今天本周的第几天
  let nowDay = now.getDate() // 当前日
  let nowMonth = now.getMonth() // 当前月
  let nowYear = now.getFullYear() // 当前年
  let weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek + 1 + 7 * num)
  return weekStartDate
}

/**
 *  获取当前周（num=0）或第num周的结束时间
 * @param num
 * @returns {Date}
 */
export function getWeekEndDate (num) {
  num = num || 0
  let now = new Date() // 当前日期
  let nowDayOfWeek = now.getDay() // 今天本周的第几天
  let nowDay = now.getDate() // 当前日
  let nowMonth = now.getMonth() // 当前月
  let nowYear = now.getFullYear() // 当前年
  let weekStartDate = new Date(nowYear, nowMonth, nowDay + (7 - nowDayOfWeek) + 7 * num)
  return weekStartDate
}

/**
 * 获取某一月的第一天
 * @param num 当前月的前n个月(n为负数)或后n个月(n为正数)或当前月(n=0)
 * @returns {Date}
 */
export function getMonthStartDate (num) {
  num = num || 0
  let now = new Date()
  let nowMonth = now.getMonth() // 当前月
  let nowYear = now.getFullYear() // 当前年
  let date = new Date(nowYear, nowMonth + num, 1)
  return date
}
/**
 * 获取某一月的最后一天
 * @param num 当前月的前n个月(n为负数)或后n个月(n为正数)或当前月(n=0)
 * @returns {Date}
 */
export function getMonthEndDate (num) {
  num = num || 0
  let now = new Date()
  let nowMonth = now.getMonth() // 当前月
  let nowYear = now.getFullYear() // 当前年
  let date = new Date(new Date(nowYear, nowMonth + num + 1, 1).getTime() - 1 * 24 * 3600 * 1000)
  return date
}

/**
 * 乘法计算
 * @param arg1 参数1
 * @param arg2 参数2
 * @returns {number}
 */
export function accMul (arg1, arg2) {
  var baseNum = 0
  try {
    baseNum += arg1.toString().split('.')[1].length
  } catch (e) {
  }
  try {
    baseNum += arg2.toString().split('.')[1].length
  } catch (e) {
  }
  return Number(arg1.toString().replace('.', '')) *
    Number(arg2.toString().replace('.', '')) /
    Math.pow(10, baseNum)
}

/**
 * 加法计算
 * @param arg1 参数1
 * @param arg2 参数2
 * @returns {number}
 */
export function accAdd (arg1, arg2) {
  var baseNum, baseNum1, baseNum2
  try {
    baseNum1 = arg1.toString().split('.')[1].length
  } catch (e) {
    baseNum1 = 0
  }
  try {
    baseNum2 = arg2.toString().split('.')[1].length
  } catch (e) {
    baseNum2 = 0
  }
  baseNum = Math.pow(10, Math.max(baseNum1, baseNum2))
  return (arg1 * baseNum + arg2 * baseNum) / baseNum
}

/**
 * 除法计算
 * @param arg1 参数1
 * @param arg2 参数2
 * @returns {number}
 */
export function accDiv (arg1, arg2) {
  var baseNum1 = 0, baseNum2 = 0
  var baseNum3, baseNum4
  try {
    baseNum1 = arg1.toString().split('.')[1].length
  } catch (e) {
    baseNum1 = 0
  }
  try {
    baseNum2 = arg2.toString().split('.')[1].length
  } catch (e) {
    baseNum2 = 0
  }
  // with (Math) {
  //     baseNum3 = Number(arg1.toString().replace(".", ""));
  //     baseNum4 = Number(arg2.toString().replace(".", ""));
  //     return (baseNum3 / baseNum4) * pow(10, baseNum2 - baseNum1);
  // }
  baseNum3 = Number(arg1.toString().replace('.', ''))
  baseNum4 = Number(arg2.toString().replace('.', ''))
  return baseNum3 / baseNum4 * Math.pow(10, baseNum2 - baseNum1)
}
/**
 * 获取当前年月日星期几
 * @param dateString 格式如：2018-02-25
 * @returns {string}
 */
export function getWeek(dateString){
    var date;
    if(isNull(dateString)){
        date = new Date();
    }else{
        var dateArray = dateString.split("-");
        date = new Date(dateArray[0], parseInt(dateArray[1] - 1), dateArray[2]);
    }
    //var weeks = new Array("日", "一", "二", "三", "四", "五", "六");
    //return "星期" + weeks[date.getDay()];
    return "周" + "日一二三四五六".charAt(date.getDay());
}


export function findActiveSets (list, path) {
  let indexStr = ''
  if (!list || (list && list.length <= 0)) {
    return indexStr
  }
  list.forEach((ele, index) => {
    // if (ele.url === path) {
    if (ele.url === path || (ele.states && ele.states.includes(path))) {
      indexStr = ele.sets
      return false
    } else if (ele.children && ele.children.length > 0) {
      let flag = ele.children.forEach((e, idx) => {
        if (e.url === path || (e.states && e.states.includes(path))) {
          indexStr = ele.sets
          return false
        }
      })
      if (!flag) {
        return false
      }
    }
  })
  return indexStr
}

export function findActiveUrl (list, path) {
  let activeUrl = ''
  if (!list || (list && list.length <= 0)) {
    return activeUrl
  }
  list.forEach((ele, index) => {
    // if (ele.url === path) {
    if (ele.url === path || (ele.states && ele.states.includes(path))) {
      activeUrl = ele.url
      return false
    } else if (ele.children && ele.children.length > 0) {
      let flag = ele.children.forEach((e, idx) => {
        if (e.url === path || (e.states && e.states.includes(path))) {
          activeUrl = ele.url
          return false
        }
      })
      if (!flag) {
        return false
      }
    }
  })
  return activeUrl
}
/**
 * 清除本地登录相关信息
 */
export function clearAuthSg () {
  removeSg('token')
  removeSg('tokenType')
  removeSg('userInfo')
  removeSg('permission')
}

/**
 * 设置本地登录相关信息
 */
export function setAuthSg (res) {
  if (!res) {
    return
  }
  setSg('token', res.token)
  setSg('tokenType', res.token_type)
  setSgObj('userInfo', res)
  setSgObj('permission', res.menu)
}

export function checkNotNull (val) {
  return typeof val !== 'undefined' && val !== '' && val !== null
}

export function transListToTree (list, pid) {
  if (!list || (list && list.length <= 0)) {
    return []
  }
  let ary = []
  list.forEach(ele => {
    if (ele.parentid === pid) {
      let childAry = transListToTree(list, ele.id)
      if (childAry && childAry.length > 0) {
        ele.child = childAry
      }
      ary.push(ele)
    }
  })
  return ary
}

export function calFileSize (size) {
  if (!checkNotNull(size)) {
    return 0
  }
  if (Number(size) < 1024 * 1024) {
    return (Math.round(Number(size) / 1024)).toFixed(2)  + 'Kb'
  } else {
    return (Math.round(Number(size) / 1024 / 1024)).toFixed(2) + 'Mb'
  }
}
export function reFixSize (size) {
  if (size.slice(-2) == 'Mb') {
    return Math.round(Number(size.slice(0, -2)) * 1024 * 1024)
  } else if (size.slice(-2) == 'Kb') {
    return Math.round(Number(size.slice(0, -2)) * 1024)
  }
}

export function isArray (o) {
  return Object.prototype.toString.call(o) === '[object Array]'
}

// -------------------------------------------------------------------------
// hybrid方法
// export function freezeWebApi () {
//   // loginOut --客户端关闭之前 调用的本地方法 headerTop为web页面自身调用关闭
//   window.closeWebPage = () => {
//     let token = getSg('token')
//     UI.confirm('您确定要退出吗？', '', token ? '' : 'login-confirm-dialog')
//       .then((res) => {
//         this.closeWindow()
//       }, (err) => {
//         console.log('取消')
//       })
//   }
// }

export function getBase  () {
  if (typeof JS_Normal !== 'undefined' && typeof eval(JS_Normal.getBase) === 'function') {
    var json = JS_Normal.getBase();
    var jsonObj = json ? JSON.parse(json) : {}
    if (jsonObj && jsonObj.code == 1) {
      return jsonObj.msg
    }
  }
  return ''
}

/**
 * 打开新窗口
 * @param obj
 */
export function openWindow (obj) {
  if (!obj) {
    return
  }
  if (typeof JS_Normal !== 'undefined' && typeof eval(JS_Normal.openNewWindow) === 'function') {
    JS_Normal.openNewWindow(obj.url, obj.width, obj.height);
  }
}

// 设置当前窗口尺寸
// win_position ：窗口位置 0 最大化，5全屏
// win_type ：显示窗口按钮显示状态 1关闭显示，2最大化显示，
//           4最小化显示，8设置按钮显示，16刷新按钮显示(显示方式为掩码)
// 返回值{"code":"1","msg":"success"}
// class function setWindow(win_width : integer;win_high : integer;win_position : integer;win_type : integer): AnsiString;
/**
 * 设置当前窗口
 * @param width 宽度
 * @param height 高度
 * @param position 位置 0最大化，5居中
 * @param type 窗口顶部按钮 1关闭显示，2最大化显示，4最小化显示，8设置按钮显示，16刷新按钮显示
 */
export function setWindow (width, height, position, type) {
  if (typeof JS_Normal !== 'undefined' && typeof eval(JS_Normal.setWindow) === 'function') {
    width = parseInt(width)
    height = parseInt(height)
    position = parseInt(position)
    type = parseInt(type)
    try {
      let res = JS_Normal.setWindow(width, height, position, type);
    } catch (e) {
      console.log(e)
    }
  }
}
/**
 * 关闭窗口
 */
export function closeWindow () {
  if (typeof JS_Normal !== 'undefined' && typeof eval(JS_Normal.setWindow) === 'function') {
    JS_Normal.quit()
  }
}
/**
 * 使用默认浏览器打开一个url接口
 * @param obj
 */
export function openDelbrowser (url) {
  if (!url) {
    return
  }
  if (typeof JS_Normal !== 'undefined' && typeof eval(JS_Normal.openDelbrowser) === 'function') {
    let host = window.location.host
    if (url.indexOf('http://') === -1) {
      url = 'http://' + host + url
    }
    JS_Normal.openDelbrowser(url)
  } else if (typeof window !== 'undefined') {
    window.open(url)
  }
}

/**
 * 判断浏览器环境
 * @param obj
 */

 export function isMobile () {
  let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)
  return flag;
 }

 // 判断操作系统
 export function judgeOS () {
   let isWin = /windows|win32/i.test(navigator.userAgent);
   let isMac = /macintosh|mac os x/i.test(navigator.userAgent);


   if (isWin) {
     return 1
   } else if (isMac) {
     return 2
   } else {
     return 3
   }
 }
