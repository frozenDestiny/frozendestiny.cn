import axios from 'axios'
import qs from 'qs'
import { HTTP_SUCCESS_CODE, HTTP_TIMEOUT, SERVER_URL } from '../config'
import * as Fun from './function'
import * as UI from './ui'
import router from '@/router'

const instance = axios.create({
  timeout: HTTP_TIMEOUT,
  baseURL: SERVER_URL
})

instance.interceptors.request.use((config) => {
  let token = Fun.getSg('token')
  let tokenType = Fun.getSg('tokenType')
  config.headers['Authorization'] = tokenType + token

  if (config.headers['is-mutile-data']) {
    config.timeout = 0 // 上传文件时，去掉超时限制timeout
    delete config.headers['is-mutile-data']
  } else {
    // config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'   // form-data
    config.headers['Content-Type'] = 'application/json;charset=UTF-8' // json
    if (config.method === 'post' || config.method === 'put') {
      // config.data = qs.stringify(config.data, { strictNullHandling: true })
      // config.data = qs.stringify(config.data)
    }
  }
  return config
}, (err) => {
  return Promise.reject(err) // 错误传参
})

instance.interceptors.response.use((res) => {
  if (res.status !== 200) { // res status检查
    checkStatus(res)
    return Promise.reject(res)
  }
  let status = res.data ? res.data.status : ''

  if (res.config.headers['Ignore-Show-Error']) {
    return res // 返回正常结果
  }
  if (!status) {
    UI.toastError('网络状态不佳，请稍后再试')
    return Promise.reject(res)
  } else if (status && status.code !== HTTP_SUCCESS_CODE) { // 业务错误处理
    return Promise.reject(res)
  }
  return res // 返回正常结果
}, (err) => {
  if (err.response) {
    Fun.clearAuthSg()
    UI.toastError(err.response.data.status.message, 3500)
    setTimeout(() => {
      router.push({ path: '/login' })
    }, 1000)
  } else {
    if (err.code === 'ECONNABORTED') {
      UI.toastError('网络状态不佳，请稍后再试')
      console.log(err)
    } else {
      if (axios.isCancel(err)) {
        console.log('请求已取消')
      } else {
        UI.toastError('网络状态不佳，请稍后再试')
        console.log(err)
      }
    }
  }
  return Promise.reject(err) // 网络异常
})

function checkStatus(res) {
  let status = res.status
  switch (status) {
    case 401: // 登录无权限 跳转登录
      Fun.clearAuthSg()
      UI.toastError('登录超时，请重新登录~', 3500)
      setTimeout(() => {
        router.push({ path: '/login' })
      }, 1000)
      break
    case 403: // 拒绝访问
      break
    case 404: // 请求地址出错
      break
    case 500: // 服务器内部错误
      // UI.toastError('网络错误，请稍后再试~')
      break
    case 502: // 网关错误
      // UI.toastError('网络错误，请稍后再试~')
      break
    case 504: // 网关超时
      // UI.toastError('网络错误，请稍后再试~')
      break
    default:
      break
  }
}

const method = (type, api, params = {}, options = {}) => {
  return new Promise((resolve, reject) => {
    instance[type](api, params, options)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err)
      })
  })
}
/**
 * @param url
 * @param params
 * @returns {Promise}
 */
export function postServer(url, params) {
  return method('post', url, params)
}

export function getServer(url, params) {
  params = params || {}
  url = Fun.parseGetParams(url, params)
  return method('get', url)
}

export function putServer(url, params) {
  return method('put', url, params)
}

export function postFormServer(url, params, options = {}) {
  return method('post', url, params,
    Object.assign(options, {
      headers: {
        'is-mutile-data': 1,
        'Content-Type': 'multipart/form-data'
      }
    }))
}

export function deleteServer(url, params) {
  return method('delete', url, params)
}
