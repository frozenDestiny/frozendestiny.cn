//验证固定电话
export function checkMobile (num) {
    let reg = /^(\(\d{3,4}\)|\d{3,4}[-| ]|\s)?\d{7,15}$/
    //let reg = /^[0-9\-]{7,15}$/
    return reg.test(num)
  }
  //验证11为数字
  export function checkPhone (num) {
    let reg = /^1\d{10}$/
    return reg.test(num)
  }
  //验证4位数字
  export function checkFourNum (num) {
    let reg = /^[0-9]\d{3}$/
    return reg.test(num)
  }
  // 验证正数 包括0
  export function checkNumber (num) {   
    let reg = /^[1-9]\d*$/
    return reg.test(num)
  }
  // 去除空格正则
  export function checkSpace (name) {
    let reg = /([^\s])/g
    return reg.test(name)
  }
  // 验证大于0的数字，最多保留一位小数
  export function checkOneNumber (num) {
    let reg = /^([1-9]\d*)(\s|$|\.\d{1,1}\b)/
    return reg.test(num)
  }
  // 验证大于0的数字，整数位不超过7位，最多保留两位小数
  export function checkSevenNumber (num) {
    let reg = /^(0|[1-9]\d{0,6})(\.\d{1,2})?$/
    return reg.test(num)
  }
  // 验证大于0且不超过7位的正整数
  export function checkIntegerNum (num) {
    let reg = /^([1-9]\d{0,5})?$/
    return reg.test(num)
  }
  // 验证金额 保留两位
  export function checkMoney (num) {   
    let reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
    return reg.test(num)
  }
  // 验证金额 保留两位 必须大于0
  export function checkMoneyPlus (num) {   
    let reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
    let result = reg.test(num)
    if (result) {
      if (Number(num) === 0) {
        result = false
      }
    }
    return result
  }
  // 验证7位整数且不超过两位小数
  export function checkIntNumber (num) {
    let reg = /^\d{1,7}(.\d{1,2})?$/
    return reg.test(num)
  }
  export function checkImage (type) {
    let reg = /\.(jpg|jpeg|png|JPG|PNG)$/
    return reg.test(type)
  }
  
  export function checkMusic (type) {
    let reg = /\.(mp3)$/
    return reg.test(type)
  }
  
  const fileType = {
    'img': /\.(jpg|jpeg|png|JPG|PNG|bmp|gif)$/,
    'word': /\.(doc|docx)$/,
    'excel': /\.(xls|xlsx)$/,
    'ppt': /\.(ppt|pptx)/,
    'txt': /\.(txt)/,
    'pdf': /\.(pdf)/,
    'mp4': /\.(mp4|avi|rmvb|rm|asf|divx|mpg|mpeg|mpe|wmv|mkv|vob|flv)/,
  }
  
  export function getFileType (fileName) {
    let type = ''
    Object.keys(fileType).forEach(key => {
      if (fileType[key].test(fileName)) {
        type = key
        return false
      }
    })
    return type || 'unknow_file'
  }
  
  export function checkExcel (suffix) {
    let reg = /\.xl(s[xmb]?|t[xm]|am)|.csv$/
    return reg.test(suffix)
  }
  //验证必须是字母或者数字
  export function checkLetterNum (str) {
    let reg = /^([0-9]*$)|([A-Za-z]+)/
    return reg.test(str)
  }
  //验证必须是整数
  export function checkPlusNum (str) {
    let reg = /^[0-9]*$/
    return reg.test(str)
  }
  //验证必须是英文字母
  export function checkLetter (str) {
    let reg = /^[A-Za-z]+/
    return reg.test(str)
  }
  //验证非汉字
  export function checkString (str) {
    let reg = /^[\u4e00-\u9fa5]+$/
    return reg.test(str)
  }
  