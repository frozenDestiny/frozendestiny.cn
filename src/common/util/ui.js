import { ElMessage, ElMessageBox, ElNotification } from 'element-plus'
import { TOAST_DURATION } from '@/common/config'

export function toastInfo(msg, duration) {
    ElMessage.info({
        showClose: true,
        message: msg,
        duration: duration || TOAST_DURATION
    })
}


export function toastSuccess(msg, duration) {
    ElMessage.success({
        showClose: true,
        message: msg,
        duration: duration || TOAST_DURATION
    })
}


export function toastError(msg, duration) {
    ElMessage.error({
        showClose: true,
        message: msg,
        duration: duration || TOAST_DURATION
    })
}


export function toastWarning(msg, duration) {
    ElMessage.warning({
        showClose: true,
        message: msg,
        duration: duration || TOAST_DURATION
    })
}


export function confirm(msg, title, customClass) {
    return ElMessageBox.confirm(msg || '是否确认操作？', title || '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        closeOnClickModal: false,
        customClass: customClass || ''
    })
}


export function alert(msg, title, customClass) {
    return ElMessageBox.alert(msg || '暂无', title || '提示', {
        confirmButtonText: '确定'
    })
}

export function changeTheme(element, className) {
    if (!element || !className) {
        return
    }
    let classString = element.className
    let newStr = classString.replace(/\b[a-z]+-theme\b/g, '')

    newStr += ' ' + className

    element.className = newStr
}

export function notify(options) {
    return ElNotification(options)
}