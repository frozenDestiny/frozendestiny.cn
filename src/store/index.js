import { createStore } from 'vuex'

export default createStore({
    state () {
        return {
            menuActiveIndex: '',   // 导航选中
        }
    },
    mutations:{
        SET_ACTIVE: (state, active) => {
            state.menuActiveIndex = active
        },
    },
    getters:{
        menuActiveIndex: state => {
            return state.menuActiveIndex
        },
    },
    actions:{
        increment(context) {
            context.commit('increment')
        }
    }
})