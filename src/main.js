import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import '@/assets/fonts/iconfont.css'
import '@/assets/fonts/iconfont.js'
import ElementPlus from "element-plus"
import 'element-plus/dist/index.css' 
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import { createPinia } from 'pinia'
import '@/router/routing-guard'

const app = createApp(App)


app.use(ElementPlus, {locale: zhCn})
app.use(router)
app.use(createPinia())
app.use(store)


app.mount('#app')





