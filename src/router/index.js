import { createRouter, createWebHashHistory } from 'vue-router'
const routes = [
    {
        path: '/app',
        component: () => import(/* webpackChunkName: "blog" */ '@/layout/AppMain.vue'),
        children: [
            {
                path: '/index',
                component: () => import(/* webpackChunkName: "blog" */ '@/views/Index.vue')
            },
            {
                path: '/articleList',
                component: () => import(/* webpackChunkName: "blog" */ '@/views/Article.vue')
            },
            {
                path: '/idea',
                component: () => import(/* webpackChunkName: "blog" */ '@/views/Idea.vue')
            },
            {
                path: '/album',
                component: () => import(/* webpackChunkName: "blog" */ '@/views/Album.vue')
            },
            {
                path: '/message-board',
                component: () => import(/* webpackChunkName: "blog" */ '@/views/MessageBoard.vue')
            }
        ]
    },
    {
        path: '/',
        redirect: '/index'
    }

]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router